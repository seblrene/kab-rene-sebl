#ifndef __PROGTEST__
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cstdint>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <unistd.h>
#include <string>
#include <memory>
#include <vector>
#include <fstream>
#include <cassert>
#include <cstring>

#include <openssl/evp.h>
#include <openssl/rand.h>

using namespace std;

struct crypto_config
{
	const char * m_crypto_function;
	std::unique_ptr<uint8_t[]> m_key;
	std::unique_ptr<uint8_t[]> m_IV;
	size_t m_key_len;
	size_t m_IV_len;
};

#endif /* _PROGTEST_ */

constexpr int BUFFER_SIZE = 4096;

/*
std::streampos getFileSize(const std::string& filename) {
	std::ifstream file("../" + filename, std::ios::binary | std::ios::ate);
	if (!file.is_open()) {
		std::cerr << "Nepodařilo se otevřít soubor: " << filename << std::endl;
		return -1;
	}
	std::streampos size = file.tellg();
	file.close();
	return size;
}
*/

bool readHeader(ifstream &iss, ofstream &oss) {
	char buffer[18];
	iss.read(buffer, 18);
	if(iss.fail() || iss.gcount() != 18) {
		//cout << "doslo k chybe pri nacitani hlavicky ze souboru" << endl;
		return false;
	}
	oss.write(buffer, 18);
	if(oss.fail()) {
		//cout << "doslo k chybe pri zapisovani hlavicky do souboru" << endl;
		return false;
	}
	return true;
}

bool checkConfig(crypto_config & config, bool encrypt) {
	if(config.m_crypto_function == nullptr || !EVP_get_cipherbyname(config.m_crypto_function)) {
		//cout << "neplatny nazev klice" << endl;
		return false;
	}
	const EVP_CIPHER * cipher = EVP_get_cipherbyname(config.m_crypto_function);
	if(cipher == nullptr) {
		//cout << "nepodarilo se nacist typ funkce" << endl;
		return false;
	}
	if(config.m_key_len < static_cast<size_t>(EVP_CIPHER_key_length(cipher)) || config.m_key == nullptr) {
		if(encrypt) {
			config.m_key_len = static_cast<size_t>(EVP_CIPHER_key_length(cipher));
			config.m_key = make_unique<uint8_t[]>(config.m_key_len);
			if(!RAND_bytes(config.m_key.get(), config.m_key_len)) {
				//cout << "nepodaril se vygenerovat novy klic" << endl;
				return false;
			}
		}else {
			//cout << "pro desifrovani nebyl vlozen klic se spravnou delkou" << endl;
			return false;
		}
	}
	if(EVP_CIPHER_iv_length(cipher) > 0) {
		if(config.m_IV_len < static_cast<size_t>(EVP_CIPHER_iv_length(cipher)) || config.m_IV == nullptr) {
			if(encrypt) {
				config.m_IV_len = static_cast<size_t>(EVP_CIPHER_iv_length(cipher));
				config.m_IV = make_unique<uint8_t[]>(config.m_IV_len);
				if(!RAND_bytes(config.m_IV.get(), config.m_IV_len)) {
					//cout << "nepodaril se vygenerovat novy IV" << endl;
					return false;
				}
			}else {
				//cout << "pro desifrovani nebyl vlozen IV se spravnou delkou" << endl;
				return false;
			}
		}
	}
	return true;
}


bool processData(const std::string & in_filename, const std::string & out_filename, crypto_config & config, bool encrypt) {
	if(in_filename.empty() || out_filename.empty() || in_filename == out_filename) {
		//cout << "neni zadany nazev vstupniho nebo vystupniho souboru nebo jsou oba nazvy stejne" << endl;
	}
	ifstream iss( in_filename, ios::binary);
	ofstream oss( out_filename, ios::binary | ios::trunc);
	if(!iss.is_open() || !oss.is_open()) {
		//cout << "vstupni nebo vystupni stream se nepodarilo otevrit" << endl;
		return false;
	}
	if(!readHeader(iss, oss)) {
		//cout << "hlavicka souboru nebyla nactena" << endl;
		return false;
	}
	if(!checkConfig(config, encrypt)) {
		return false;
	}
	int inLen, outLen;
	const EVP_CIPHER * cipher = EVP_get_cipherbyname(config.m_crypto_function);
	if(cipher == nullptr) {
		//cout << "nepodarilo se nacist typ funkce" << endl;
		return false;
	}
	uint8_t inBuffer[BUFFER_SIZE];
	uint8_t outBuffer[BUFFER_SIZE + 128];
	unique_ptr<EVP_CIPHER_CTX, decltype(&EVP_CIPHER_CTX_free)> ctx(EVP_CIPHER_CTX_new(), EVP_CIPHER_CTX_free);
	if(ctx == nullptr) {
		cout << "chyba pri vytvareni kontextu" << endl;
		return false;
	}
	if(!EVP_CipherInit_ex(ctx.get(), cipher, nullptr, config.m_key.get(), config.m_IV.get(), encrypt)) {
		//cout << "chyba pri inicializace sifrovani" << endl;
		return false;
	}
	while(true) {
		iss.read(reinterpret_cast<char*>(inBuffer), BUFFER_SIZE);
		inLen = iss.gcount();
		if(iss.fail() && !iss.eof()) {
			//cout << "doslo k chybe pri nacitani ze souboru" << endl;
			return false;
		}
		if(inLen == 0) {
			break;
		}
		if(!EVP_CipherUpdate(ctx.get(), outBuffer, &outLen, inBuffer, inLen)) {
			//cout << "chyba pri sifrovani jednotlivych bloku" << endl;
			return false;
		}
		oss.write(reinterpret_cast<char*>(outBuffer), outLen);
		if(oss.fail()) {
			//cout << "doslo k chybe pri zapisovani do souboru" << endl;
			return false;
		}
	}
	if(!EVP_CipherFinal_ex(ctx.get(), outBuffer, &outLen)) {
		//cout << "chyba pri sifrovani posledniho bloku" << endl;
		return false;
	}
	oss.write(reinterpret_cast<char*>(outBuffer), outLen);
	if(oss.fail()) {
		//cout << "doslo k chybe pri zapisovani do souboru" << endl;
		return false;
	}
	return true;
}


bool encrypt_data ( const std::string & in_filename, const std::string & out_filename, crypto_config & config )
{
	if(!processData(in_filename, out_filename, config, true)) {
		cout << "soubor se nepodarilo zasifrovat" << endl;
		return false;
	}
	return true;
}

bool decrypt_data ( const std::string & in_filename, const std::string & out_filename, crypto_config & config )
{
	if(!processData(in_filename, out_filename, config, false)) {
		cout << "soubor se nepodarilo desifrovat" << endl;
		return false;
	}
	return true;
}


#ifndef __PROGTEST__


vector<char> readFiletoVec(const string &name) {
	ifstream iss(name, ios::binary | ios::ate);
	if(!iss.is_open()) {
		throw::runtime_error("soubor pro kontrolu dat nelze otevrit");
	}
	streamsize fileSize = iss.tellg();
	iss.seekg(0, ios::beg);
	vector<char> buffer(fileSize);
	iss.read(buffer.data(), fileSize);
	return buffer;
}

bool compare_files ( const char * name1, const char * name2 )
{
	vector<char> file1 = readFiletoVec(name1);
	vector<char> file2 = readFiletoVec(name2);
	if(file1.size() != file2.size()) {
		cout << "velikost souboru je: " << file1.size() << endl;
		cout << "velikost kontrolniho souboru je: " << file2.size() << endl;
		cout << "soubory: " << name1 << " a " << name2 << " nemaji stejnou velikost" << endl;
		return false;
	}
	for(auto it1 = file1.begin(), it2 = file2.begin(); it1 != file1.end(); ++it1, ++it2) {
		if(*it1 != *it2) {
			cout << "soubory: " << name1 << " a " << name2 << " se neshoduji" << endl;
			return false;
		}
	}
	cout << "soubory: " << name1 << " a " << name2 << " se shoduji" << endl;
	return true;
}

int main ( void )
{
	crypto_config config {nullptr, nullptr, nullptr, 0, 0};

	// ECB mode
	config.m_crypto_function = "AES-128-ECB";
	config.m_key = std::make_unique<uint8_t[]>(16);
 	memset(config.m_key.get(), 0, 16);
	config.m_key_len = 16;

	assert( encrypt_data  ("homer-simpson.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "homer-simpson_enc_ecb.TGA") );

	assert( decrypt_data  ("homer-simpson_enc_ecb.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "homer-simpson.TGA") );

	assert( encrypt_data  ("UCM8.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "UCM8_enc_ecb.TGA") );

	assert( decrypt_data  ("UCM8_enc_ecb.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "UCM8.TGA") );

	assert( encrypt_data  ("image_1.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "ref_1_enc_ecb.TGA") );

	assert( encrypt_data  ("image_2.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "ref_2_enc_ecb.TGA") );

	assert( decrypt_data ("image_3_enc_ecb.TGA", "out_file.TGA", config)  &&
		    compare_files("out_file.TGA", "ref_3_dec_ecb.TGA") );

	assert( decrypt_data ("image_4_enc_ecb.TGA", "out_file.TGA", config)  &&
		    compare_files("out_file.TGA", "ref_4_dec_ecb.TGA") );

	// CBC mode
	config.m_crypto_function = "AES-128-CBC";
	config.m_IV = std::make_unique<uint8_t[]>(16);
	config.m_IV_len = 16;
	memset(config.m_IV.get(), 0, 16);

	assert( encrypt_data  ("UCM8.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "UCM8_enc_cbc.TGA") );

	assert( decrypt_data  ("UCM8_enc_cbc.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "UCM8.TGA") );

	assert( encrypt_data  ("homer-simpson.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "homer-simpson_enc_cbc.TGA") );

	assert( decrypt_data  ("homer-simpson_enc_cbc.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "homer-simpson.TGA") );

	assert( encrypt_data  ("image_1.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "ref_5_enc_cbc.TGA") );

	assert( encrypt_data  ("image_2.TGA", "out_file.TGA", config) &&
			compare_files ("out_file.TGA", "ref_6_enc_cbc.TGA") );

	assert( decrypt_data ("image_7_enc_cbc.TGA", "out_file.TGA", config)  &&
		    compare_files("out_file.TGA", "ref_7_dec_cbc.TGA") );

	assert( decrypt_data ("image_8_enc_cbc.TGA", "out_file.TGA", config)  &&
		    compare_files("out_file.TGA", "ref_8_dec_cbc.TGA") );

	//-----------------------------------------------------------------
	//My test
	const string testFile = "test1.bin";
	ofstream oss(testFile, ios::binary | ios::trunc);
	if(!oss.is_open()) {
		cout << "testovaci soubor se neotevrel" << endl;
		return 1;
	}
	oss.write("texttexttexttext", 16);
	if(oss.fail()) {
		cout << "zapis do souboru selhal" << endl;
	}
	oss.close();
	//cout << "velikost zasifrovaneho souboru test1.bin: " << getFileSize(testFile) << endl;
	assert( !encrypt_data("test1.bin", "out_file.TGA", config));
	assert( !encrypt_data("", "out_file.TGA", config));
	return 0;
}

#endif /* _PROGTEST_ */