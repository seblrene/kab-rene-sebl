#ifndef __PROGTEST__
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <climits>
#include <cstdint>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <string_view>
#include <memory>
#include <vector>
#include <fstream>
#include <cassert>
#include <cstring>

#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/pem.h>

using namespace std;

#endif /* __PROGTEST__ */

constexpr int BUFF_SIZE = 4096;

void closeStreams(ifstream &iss, ofstream &oss) {
    iss.close();
    oss.close();
}

bool encryptBinData(ifstream &iss, ofstream &oss, unique_ptr<EVP_CIPHER_CTX, decltype(&EVP_CIPHER_CTX_free)> ctx) {
    uint8_t inBuffer[BUFF_SIZE];
    uint8_t outBuffer[BUFF_SIZE + EVP_MAX_BLOCK_LENGTH];
    int inLen = 0, outLen = 0;

    while(true) {
        iss.read(reinterpret_cast<char*>(inBuffer), sizeof(inBuffer));
        inLen = iss.gcount();
        if(iss.fail() && !iss.eof()) {
            return false;
        }
        if(inLen == 0) {
            break;
        }
        if(!EVP_SealUpdate(ctx.get(), outBuffer, &outLen, inBuffer, inLen)) {
            return false;
        }
        oss.write(reinterpret_cast<char*>(outBuffer), outLen);
        if(oss.fail()) {
            return false;
        }
    }
    if(!EVP_SealFinal(ctx.get(), outBuffer, &outLen)) {
        return false;
    }
    oss.write(reinterpret_cast<char*>(outBuffer), outLen);
    if(oss.fail()) {
        return false;
    }
    return true;
}

bool encryptData(string_view inFile, string_view outFile, string_view publicKeyFile, string_view symmetricCipher) {
    ifstream iss(inFile.data(), ios::binary);
    ofstream oss(outFile.data(), ios::binary | ios::trunc);
    if(!iss.is_open() || !oss.is_open()) {
        return false;
    }

    const EVP_CIPHER *cipher = EVP_get_cipherbyname(symmetricCipher.data());
    if(!cipher) {
        closeStreams(iss, oss);
        return false;
    }

    unique_ptr<EVP_CIPHER_CTX, decltype(&EVP_CIPHER_CTX_free)> ctx(EVP_CIPHER_CTX_new(), EVP_CIPHER_CTX_free);
    if(!ctx) {
        closeStreams(iss, oss);
        return false;
    }

    FILE *fp = fopen(publicKeyFile.data(), "r");
    if(!fp) {
        closeStreams(iss, oss);
        return false;
    }
    EVP_PKEY* pubKey = PEM_read_PUBKEY(fp, nullptr, nullptr, nullptr);
    fclose(fp);
    if(!pubKey) {
        closeStreams(iss, oss);
        return false;
    }

    unsigned char *ek = (unsigned char *) malloc(EVP_PKEY_size(pubKey));
    unsigned char iv[EVP_MAX_IV_LENGTH];
    int ekl;
    if(!EVP_SealInit(ctx.get(), cipher, &ek, &ekl, iv, &pubKey, 1)) {
        closeStreams(iss, oss);
        EVP_PKEY_free(pubKey);
        free(ek);
        return false;
    }

    int nid = EVP_CIPHER_CTX_nid(ctx.get());
    if(nid == NID_undef) {
        closeStreams(iss, oss);
        EVP_PKEY_free(pubKey);
        free(ek);
        return false;
    }
    if(!oss.write(reinterpret_cast<const char *>(&nid), sizeof(nid)) ||
       !oss.write(reinterpret_cast<const char *>(&ekl), sizeof(ekl)) ||
       !oss.write(reinterpret_cast<char*>(ek), ekl)) {
        closeStreams(iss, oss);
        EVP_PKEY_free(pubKey);
        free(ek);
        return false;
    }

    int ivLen = EVP_CIPHER_get_iv_length(cipher);
    if(ivLen > 0) {
        if(!oss.write(reinterpret_cast<char*>(iv), ivLen)){
            closeStreams(iss, oss);
            EVP_PKEY_free(pubKey);
            free(ek);
            return false;
        }
    }

    if(!encryptBinData(iss, oss, move(ctx))) {
        closeStreams(iss, oss);
        EVP_PKEY_free(pubKey);
        free(ek);
        return false;
    }

    closeStreams(iss, oss);
    EVP_PKEY_free(pubKey);
    free(ek);
    return true;
}

bool seal( string_view inFile, string_view outFile, string_view publicKeyFile, string_view symmetricCipher )
{
    if(inFile.empty() || outFile.empty() || publicKeyFile.empty() || symmetricCipher.empty() || inFile == outFile) {
        if(!outFile.empty()) {
            remove(outFile.data());
        }
        return false;
    }
    if(!encryptData(inFile, outFile, publicKeyFile, symmetricCipher)) {
        if(!outFile.empty()) {
            remove(outFile.data());
        }
        return false;
    }
    return true;
}

bool decryptBinData(ifstream &iss, ofstream &oss, unique_ptr<EVP_CIPHER_CTX, decltype(&EVP_CIPHER_CTX_free)> ctx) {
    uint8_t inBuffer[BUFF_SIZE];
    uint8_t outBuffer[BUFF_SIZE + EVP_MAX_BLOCK_LENGTH];
    int inLen, outLen;

    while(true) {
        iss.read(reinterpret_cast<char*>(inBuffer), sizeof(inBuffer));
        inLen = iss.gcount();
        if(iss.fail() && !iss.eof()) {
            return false;
        }
        if(inLen == 0) {
            break;
        }
        if(!EVP_OpenUpdate(ctx.get(), outBuffer, &outLen, inBuffer, inLen)) {
            return false;
        }
        if(!oss.write(reinterpret_cast<char*>(outBuffer), outLen)) {
            return false;
        }
    }

    if(!EVP_OpenFinal(ctx.get(), outBuffer, &outLen)) {
        return false;
    }

    if(!oss.write(reinterpret_cast<char*>(outBuffer), outLen)){
        return false;
    }

    return true;
}

bool decryptData(string_view inFile, string_view outFile, string_view privateKeyFile) {
    int nid, encrypedSymKeyLen;
    ifstream iss(inFile.data(), ios::binary);
    ofstream oss(outFile.data(), ios::binary | ios::trunc);
    if(!iss.is_open() || !oss.is_open()) {
        return false;
    }

    if(!iss.read(reinterpret_cast<char*>(&nid), sizeof(nid)) ||
       !iss.read(reinterpret_cast<char*>(&encrypedSymKeyLen), sizeof(encrypedSymKeyLen)) ||
       encrypedSymKeyLen <= 0 ) {
        closeStreams(iss, oss);
        return false;
    }

    const EVP_CIPHER *cipher = EVP_get_cipherbynid(nid);
    if(!cipher) {
        closeStreams(iss, oss);
        return false;
    }

    unique_ptr<EVP_CIPHER_CTX, decltype(&EVP_CIPHER_CTX_free)> ctx (EVP_CIPHER_CTX_new(), EVP_CIPHER_CTX_free);
    if(!ctx) {
        closeStreams(iss, oss);
        return false;
    }

    unsigned char *encrypedSymKey = (unsigned char*) malloc(encrypedSymKeyLen);
    if(!iss.read(reinterpret_cast<char*>(encrypedSymKey) , encrypedSymKeyLen)) {
        closeStreams(iss, oss);
        free(encrypedSymKey);
        return false;
    }

    unsigned char iv[EVP_MAX_IV_LENGTH];
    size_t ivLen = EVP_CIPHER_get_iv_length(cipher);
    if(ivLen > 0) {
        if(!iss.read(reinterpret_cast<char*>(iv), ivLen)) {
            closeStreams(iss, oss);
            free(encrypedSymKey);
            return false;
        }
    }

    FILE *fp = fopen(privateKeyFile.data(), "r");
    if(!fp) {
        closeStreams(iss, oss);
        free(encrypedSymKey);
        return false;
    }
    EVP_PKEY *privateKey = PEM_read_PrivateKey(fp, nullptr, nullptr, nullptr);
    fclose(fp);
    if(!privateKey) {
        closeStreams(iss, oss);
        free(encrypedSymKey);
        return false;
    }

    if(!EVP_OpenInit(ctx.get(), cipher, encrypedSymKey, encrypedSymKeyLen, iv, privateKey)) {
        closeStreams(iss, oss);
        free(encrypedSymKey);
        EVP_PKEY_free(privateKey);
        return false;
    }

    if(!decryptBinData(iss, oss, move(ctx))) {
        closeStreams(iss, oss);
        free(encrypedSymKey);
        EVP_PKEY_free(privateKey);
        return false;
    }

    closeStreams(iss, oss);
    EVP_PKEY_free(privateKey);
    free(encrypedSymKey);
    return true;
}

bool open( string_view inFile, string_view outFile, string_view privateKeyFile )
{
    if(inFile.empty() || outFile.empty() || privateKeyFile.empty() || inFile == outFile) {
        if(!outFile.empty()) {
            remove(outFile.data());
        }
        return false;
    }
    if(!decryptData(inFile, outFile, privateKeyFile)) {
        if(!outFile.empty()) {
            remove(outFile.data());
        }
        return false;
    }
    return true;
}



#ifndef __PROGTEST__

bool cmpFile(string_view file1, string_view file2) {
    ifstream iss1(file1.data(), ios::binary);
    ifstream iss2(file2.data(), ios::binary);
    if(!iss1.is_open() || !iss2.is_open()) {
        cout << "kontrolni soubory se nepodarilo otevrit" << endl;
        return false;
    }
    size_t iss1Size = iss1.tellg(), iss2Size = iss2.tellg();
    vector<char> vec1(iss1Size);
    iss1.seekg(0, ios::beg);
    iss1.read(vec1.data(), iss1Size);
    vector<char> vec2(iss2Size);
    iss2.seekg(0,ios::beg);
    iss2.read(vec2.data(), iss2Size);
    if(iss1.fail() || iss2.fail()) {
        cout << "problem s nactenim vstup z dodanych souboru" << endl;
        return false;
    }
    if(vec1 != vec2) {
        cout << "soubory nejsou stejne" << endl;
        return false;
    }
    return true;
}

int main ( void )
{
    assert( seal("fileToEncrypt.txt", "sealed.bin", "PublicKey.pem", "aes-128-cbc") );
    assert( open("sealed.bin", "openedFileToEncrypt.txt", "PrivateKey.pem") );

    assert(cmpFile("fileToEncrypt.txt", "openedFileToEncrypt.txt"));

    assert( open("sealed_sample.bin", "opened_sample.txt", "PrivateKey.pem") );

    assert( !seal("", "sealed.bin", "PublicKey.pem", "aes-128-cbc") );
    assert( !seal("fileToEncrypt.txt", "", "PublicKey.pem", "aes-128-cbc") );
    assert( !seal("fileToEncrypt.txt", "sealed.bin", "PublicKey.pem", "") );
    assert( seal("fileToEncrypt.txt", "sealed.bin", "PublicKey.pem", "aes-128-ecb") );
    assert( open("sealed.bin", "openedFileToEncrypt.txt", "PrivateKey.pem") );
    return 0;
}

#endif /* __PROGTEST__ */