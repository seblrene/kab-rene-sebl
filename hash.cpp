#ifndef __PROGTEST__
#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <string>
#include <string_view>
#include <vector>

#include <openssl/evp.h>
#include <openssl/rand.h>

using namespace std;

#endif /* __PROGTEST__ */

int SIZE = 64;

bool checkZeroBits(const int numberZeroBits, const vector<unsigned char> &hash) {
    int bitsChecked = 0;
    for(const unsigned char &byte : hash) {
        int n = static_cast<int> (byte);
        if(n == 0) {
            bitsChecked += 8;
        }else if(n == 1) {
            bitsChecked += 7;
            break;
        }else if(n >= 2 && n <= 3) {
            bitsChecked += 6;
            break;
        }else if(n >= 4 && n <= 7) {
            bitsChecked += 5;
            break;
        }else if(n >= 8 && n <= 15) {
            bitsChecked += 4;
            break;
        }else if(n >= 16 && n <= 31) {
            bitsChecked += 3;
            break;
        }else if(n >= 32 && n <= 63) {
            bitsChecked += 2;
            break;
        }else if(n >= 64 && n <= 127) {
            bitsChecked += 1;
            break;
        }else {
            break;
        }
    }
    return bitsChecked >= numberZeroBits;
}

string convertToHex(const vector<unsigned char> &data) {
    stringstream ss;
    for(unsigned char c : data) {
        ss << hex << setw(2) << setfill('0') << (int) c;
    }
    return ss.str();
}

int findHash (int numberZeroBits, string & outputMessage, string & outputHash) {
    if(numberZeroBits < 0 || numberZeroBits > 512) {
        return 0;
    }
    srand(time(nullptr));
    unsigned int lenghtOfHash;
    vector<unsigned char> message(SIZE);
    vector<unsigned char> hash(SIZE);
    EVP_MD_CTX * context = EVP_MD_CTX_new();
    if(context == nullptr) {
        return 0;
    }
    if(RAND_bytes(message.data(), SIZE) != 1) {
        cout << "Chyba pri generovani nahodnych dat" << endl;
        EVP_MD_CTX_free(context);
        return 0;
    }
    while(true) {
        if(EVP_DigestInit_ex(context, EVP_sha512(), nullptr) != 1) {
            EVP_MD_CTX_free(context);
            return 0;
        }
        if(EVP_DigestUpdate(context, message.data(), message.size()) != 1) {
            EVP_MD_CTX_free(context);
            return 0;
        }
        if(EVP_DigestFinal_ex(context, hash.data(), &lenghtOfHash) != 1) {
            EVP_MD_CTX_free(context);
            return 0;
        }
        if(checkZeroBits(numberZeroBits, hash)) {
            break;
        }
        int idx = rand() % SIZE;
        message[idx] ^= hash[idx];
    }
    outputMessage = convertToHex(message);
    outputHash = convertToHex(hash);
    EVP_MD_CTX_free(context);
    return 1;
}

int findHashEx (int numberZeroBits, string & outputMessage, string & outputHash, string_view hashType) {
    /* TODO or use dummy implementation */
    return 1;
}

#ifndef __PROGTEST__

int checkHash(int bits, const string & hash) {
    int n = 0;
    bool f = false;
    for (char hexDigit : hash) {
        int value = (hexDigit >= '0' && hexDigit <= '9') ? hexDigit - '0' :
                    (hexDigit >= 'a' && hexDigit <= 'f') ? hexDigit - 'a' + 10 :
                    (hexDigit >= 'A' && hexDigit <= 'F') ? hexDigit - 'A' + 10 : 0;
        for (int i = 3; i >= 0; --i) {
            if ((value >> i) & 1) {
                f = true;
                break;
            } else {
                ++n;
            }
        }
        if(f) {
            break;
        }
    }
    cout << "pocet pozadovanych pocatecnich nulovych bitu: " << bits << endl;
    cout << "pocet vsech pocatecnich nulovych bitu: " << n << endl;
    cout << "hash: " << hash  << "\n" << endl;
    return n >= bits;
}

int main (void) {
    string hash, message;
    assert(findHash(0, message, hash) == 1);
    assert(!message.empty() && !hash.empty() && checkHash(0, hash));
    message.clear();
    hash.clear();
    assert(findHash(1, message, hash) == 1);
    assert(!message.empty() && !hash.empty() && checkHash(1, hash));
    message.clear();
    hash.clear();
    assert(findHash(2, message, hash) == 1);
    assert(!message.empty() && !hash.empty() && checkHash(2, hash));
    message.clear();
    hash.clear();
    assert(findHash(3, message, hash) == 1);
    assert(!message.empty() && !hash.empty() && checkHash(3, hash));
    message.clear();
    hash.clear();
    assert(findHash(10, message, hash) == 1);
    assert(!message.empty() && !hash.empty() && checkHash(10, hash));
    message.clear();
    hash.clear();
    assert(findHash(15, message, hash) == 1);
    assert(!message.empty() && !hash.empty() && checkHash(15, hash));
    message.clear();
    hash.clear();
    assert(findHash(25, message, hash) == 1);
    assert(!message.empty() && !hash.empty() && checkHash(25, hash));
    message.clear();
    hash.clear();
    assert(findHash(7, message, hash) == 1);
    assert(!message.empty() && !hash.empty() && checkHash(7, hash));
    message.clear();
    hash.clear();
    assert(findHash(17, message, hash) == 1);
    assert(!message.empty() && !hash.empty() && checkHash(17, hash));
    message.clear();
    hash.clear();
    assert(findHash(-1, message, hash) == 0);
    assert(findHash(513, message, hash) == 0);

    //my test

    return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */